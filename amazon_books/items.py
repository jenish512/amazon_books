# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AmazonBooksItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    title = scrapy.Field()
    pagelink = scrapy.Field()
    isbn_10 = scrapy.Field()
    isbn_13 = scrapy.Field()
    category = scrapy.Field()
    selling_url = scrapy.Field()
    selling_vendor = scrapy.Field()
    selling_price = scrapy.Field()
    buying_vendor = scrapy.Field()
    buying_url = scrapy.Field()
    condition = scrapy.Field()
    buying_descr = scrapy.Field()
    buying_cost_includnig_shipping = scrapy.Field()
    pass
