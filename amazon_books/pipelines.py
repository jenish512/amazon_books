# -*- coding: utf-8 -*-
# Define your item pipelines here
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
from scrapy import signals
from scrapy.exporters import CsvItemExporter
import sys
import shutil
import MySQLdb


class AmazonBooksPipeline(object):

    def __init__(self):
        self.isbn_13s_seen = set()
        self.conn = MySQLdb.connect(host='localhost', user='root', passwd='root', db='amazon_books', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        self.file = open('output.csv', 'w+b')
        self.exporter = CsvItemExporter(self.file)
        self.exporter.fields_to_export = ['title','category','pagelink','isbn_10','isbn_13','buying_vendor', 'buying_url', 'condition', 'buying_descr', 'buying_cost_includnig_shipping', 'selling_url', 'selling_vendor', 'selling_price']
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        self.file.close()
        self.conn.close()
        shutil.rmtree('crawls/books')


    def process_item(self, item, spider):
        
        count = 0
        count = self.cursor.execute("""SELECT * FROM `books_tradein` WHERE `isbn_13`=%s""",[item['isbn_13']])
        if count > 0:
            print "\nBook already exist in database...\n"
        else:
            print "Adding book to the database"
            try:
                self.cursor.execute("""INSERT INTO `books_tradein`(`title`, `category`, `isbn_13`, `isbn_10`, `amzn_url`) VALUES (%s, %s, %s, %s, %s)""",(item['title'], item['category'] ,item['isbn_13'], item['isbn_10'], item['pagelink']))
                self.conn.commit()

            except MySQLdb.Error as e:
                print "Error :%s" % e
                pass

        # self.exporter.export_item(item)
        # return item
        # Processing CSV file..
        # Making unique checker based on isbn_13 and selling vendors...(because single book can have multiple selling vendors)
        dupli_checker = item['isbn_13'] + ' - ' + item['selling_vendor'] + ' - ' + item['buying_url'] + ' - ' + item['buying_cost_includnig_shipping']
        if dupli_checker in self.isbn_13s_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.isbn_13s_seen.add(dupli_checker)
            self.exporter.export_item(item)
            return item