import scrapy

class BooksSpider(scrapy.Spider):
    AUTOTHROTTLE_ENABLED = True
    name = 'books_all'
    start_urls = ['https://www.amazon.com/s/ref=nb_sb_noss_1?url=field-keywords=textbooks']

    def parse(self,response):
        books_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/li/span/a[@class="a-link-normal s-ref-text-link" and span[text()="Books"]]/@href'

        link = response.xpath(books_sel).extract_first()
        link = response.urljoin(link)
        yield scrapy.Request(link, callback=self.parse_cat_and_books)


    def parse_cat_and_books(self, response):
        
        set_1_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/li/span/a[@class="a-link-normal s-ref-text-link"]'
        set_2_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/div[@aria-expanded="false"]/li/span/a[@class="a-link-normal s-ref-text-link"]'
        book_div_Sel = './/div[@class="s-item-container"]/div[@class="a-fixed-left-grid"]/div/div[@class="a-fixed-left-grid-col a-col-right"]'

        requests = []
        if response.xpath(set_1_sel):
            print '\nfound categories..\nGoing to categories..\n'

            
            category_pane = response.xpath(set_1_sel)
            for a in category_pane:
                link = a.xpath('./@href').extract_first()
                link = response.urljoin(link)
                req = scrapy.Request(link, callback=self.parse_cat_and_books)
                category_sel = './/li[@class="s-ref-indent-one"]/span/h4/text()'
                category = response.xpath(category_sel).extract_first()
                print category
                print("Going into a category")
                requests.append(req)

            if response.xpath(set_2_sel).extract_first():
                category_pane2 = response.xpath(set_2_sel)
                for a in category_pane2:
                    link = a.xpath('./@href').extract_first()
                    link = response.urljoin(link)
                    req = scrapy.Request(link, callback=self.parse_cat_and_books)
                    print("Going into a category")
                    requests.append(req)

            for req in requests:
                yield req
                    

        else:
            print '\nNo more categories found..\nGetting books info now..\n'


            category_sel = './/li[@class="s-ref-indent-one"]/span/h4/text()'
            category_count = './/div[@class="s-first-column"]/h1/text()'
            category = response.xpath(category_sel).extract_first()
            category_count = response.xpath(category_count).extract_first()

            # match_count = re.match(r'of ([0-9]+) results for', category_count, re.I|re.M)
            # if match_count:
            #     category_count = match_count.group(1)
            # yield {
            #     'category_count' : category_count,
            #     'category' : category,
            # }
            book_divs = response.xpath(book_div_Sel)
            for div in book_divs:
                isbn_10_sel = './/div/div[@class="a-column a-span5 a-span-last"]/div/div/div/div[span[text()="ISBN-10:"]]/following-sibling::div/span/text()'
                isbn_13_sel = './/div/div[@class="a-column a-span5 a-span-last"]/div/div/div/div[span[text()="ISBN-13:"]]/following-sibling::div/span/text()'
                title = div.xpath('.//div/div/a/@title').extract_first()
                pagelink = div.xpath('.//div/div/a/@href').extract_first()
                isbn_10 = div.xpath(isbn_10_sel).extract_first()
                isbn_13 = div.xpath(isbn_13_sel).extract_first()
                if title:
                    title = title.encode('utf8')
                else:
                    title = 'No title found'
                yield {
                    'title' : title,
                    'pagelink' : str(pagelink),
                    'isbn_10' : str(isbn_10),
                    'isbn_13' : str(isbn_13),
                    'category' : str(category),

                }

            # Next page for more books on amazon
            next_sel = './/span[@class="pagnRA"]/a[@title="Next Page"]/@href'
            next_page = response.xpath(next_sel).extract_first()
            if next_page:
                print '\nGoing into Next page..\n'
                next_page_link = response.urljoin(next_page)
                yield scrapy.Request(next_page_link, callback=self.parse)
