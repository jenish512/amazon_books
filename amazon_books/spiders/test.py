import scrapy
import json
import re

class BooksSpider(scrapy.Spider):
    AUTOTHROTTLE_ENABLED = True
    name = 'test'
    start_urls = ['https://www.bookfinder.com/buyback/affiliate/9781423139102.mhtml']

    def response_is_ban(self, request, response):
        pass

    def exception_is_ban(self, request, exception):
        return None

    # def parse(self,response):
    #     books_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/li/span/a[@class="a-link-normal s-ref-text-link" and span[text()="Books"]]/@href'

    #     link = response.xpath(books_sel).extract_first()
    #     link = response.urljoin(link)
    #     yield scrapy.Request(link, callback=self.parse_cat_and_books)

    # def parse_cat_and_books(self, response):
        
    #     set_1_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/li/span/a[@class="a-link-normal s-ref-text-link"]'
    #     set_2_sel = './/ul[@class="a-unordered-list a-nostyle a-vertical s-ref-indent-two"]/div[@aria-live="polite"]/div[@aria-expanded="false"]/li/span/a[@class="a-link-normal s-ref-text-link"]'

    #     book_div_Sel = './/div[@class="s-item-container"]/div[@class="a-fixed-left-grid"]/div/div[@class="a-fixed-left-grid-col a-col-right"]'

    #     file = open('data.html', 'w')
    #     file.write(response.body)
    #     print response.xpath(set_1_sel).extract_first()

    #     if response.xpath(set_1_sel):
    #         print '\nfound categories..\nGoing to categories..\n'

    #         for a in response.xpath(set_1_sel):
    #             link = a.xpath('./@href').extract_first()
    #             link = response.urljoin(link)
    #             req = scrapy.Request(link, callback=self.parse_cat_and_books)
    #             category_sel = './/li[@class="s-ref-indent-one"]/span/h4/text()'
    #             category = response.xpath(category_sel).extract_first()
    #             print category
    #             print("Going into a category")
    #             yield req

    #         if response.xpath(set_2_sel).extract_first():
    #             for a in response.xpath(set_2_sel):
    #                 link = a.xpath('./@href').extract_first()
    #                 link = response.urljoin(link)
    #                 req = scrapy.Request(link, callback=self.parse_cat_and_books)
    #                 print("Going into a category")
    #                 yield req

    #     else:
    #         print '\nNo more categories found..\nGetting books info now..\n'


    #         category_sel = './/li[@class="s-ref-indent-one"]/span/h4/text()'
    #         category_count = './/div[@class="s-first-column"]/h1/text()'
    #         category = response.xpath(category_sel).extract_first()
    #         category_count = response.xpath(category_count).extract_first()

    #         # match_count = re.match(r'of ([0-9]+) results for', category_count, re.I|re.M)
    #         # if match_count:
    #         #     category_count = match_count.group(1)
    #         # yield {
    #         #     'category_count' : category_count,
    #         #     'category' : category,
    #         # }
    #         for div in response.xpath(book_div_Sel):
    #             isbn_10_sel = './/div/div[@class="a-column a-span5 a-span-last"]/div/div/div/div[span[text()="ISBN-10:"]]/following-sibling::div/span/text()'
    #             isbn_13_sel = './/div/div[@class="a-column a-span5 a-span-last"]/div/div/div/div[span[text()="ISBN-13:"]]/following-sibling::div/span/text()'
    #             title = div.xpath('.//div/div/a/@title').extract_first()
    #             pagelink = div.xpath('.//div/div/a/@href').extract_first()
    #             isbn_10 = div.xpath(isbn_10_sel).extract_first()
    #             isbn_13 = div.xpath(isbn_13_sel).extract_first()
    #             if title:
    #                 title = title.encode('utf8')
    #             else:
    #                 title = 'No title found'
    #             item = {
    #                 'title' : title,
    #                 'pagelink' : str(pagelink),
    #                 'isbn_10' : str(isbn_10),
    #                 'isbn_13' : str(isbn_13),
    #                 'category' : str(category),

    #             }

    #             # yield item

    #             # Going to bookfinder
    #             # bkfndr_url = 'https://www.bookfinder.com/search/?author=&title=&lang=en&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr&isbn=' + str(isbn_10)
    #             bkfndr_bb_url = 'https://www.bookfinder.com/buyback/affiliate/' +str(isbn_13) + '.mhtml'
    #             bkfndr_req = scrapy.Request(bkfndr_bb_url, meta={'item':item}, callback=self.bookfinder_buyback)
    #             yield bkfndr_req



    #         # Next page for more books on amazon
    #         next_sel = './/span[@class="pagnRA"]/a[@title="Next Page"]/@href'
    #         next_page = response.xpath(next_sel).extract_first()
    #         if next_page:
    #             print '\nGoing into Next page..\n'
    #             next_page_link = response.urljoin(next_page)
    #             yield scrapy.Request(next_page_link, callback=self.parse)



    def parse(self, response):
        # Buyback url is - https://www.bookfinder.com/buyback/affiliate/<isbn_13>.mhtml
        data = json.loads(response.body)
        if 'offers' in data:
            seller_items = []
            for i in data['offers']:
                try:
                    
                    price = data['offers'][i]['offer']
                
                except KeyError as e:
                    print 'price not found'
                    
                else:
                    
                    seller_item = {
                    'selling_url' : data['offers'][i]['url'],
                    'selling_vendor' : data['offers'][i]['bookstore_display'],
                    'selling_price' : price,
                    }
                    seller_items.append(seller_item)

            isbn_10 = '1423139100'
            item = {
                'title' : '',
                'pagelink' : '',
                'isbn_10' : isbn_10,
                'isbn_13' : '',
                'category' : '',
                'seller_items' : seller_items
            }

            bkfndr_url = 'https://www.bookfinder.com/search/?author=&title=&lang=en&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr&isbn=' + str(isbn_10)
            yield scrapy.Request(bkfndr_url, meta={'item':item}, callback=self.bookfinder)
                    
        else:

            isbn_10 = '1423139100'
            item = {
                'title' : '',
                'pagelink' : '',
                'isbn_10' : isbn_10,
                'isbn_13' : '',
                'category' : '',
                'seller_items' : []
            }

            bkfndr_url = 'https://www.bookfinder.com/search/?author=&title=&lang=en&new_used=*&destination=us&currency=USD&mode=basic&st=sr&ac=qr&isbn=' + str(isbn_10)
            yield scrapy.Request(bkfndr_url, meta={'item':item}, callback=self.bookfinder)


    def bookfinder(self, response):

        NEW_BOOK_SEL = './/h3[@class="results-section-heading" and text()="New books: "]/following-sibling::table/tr[@valign]'
        USED_BOOK_SEL = './/h3[@class="results-section-heading" and text()="Used books: "]/following-sibling::table/tr[@valign]'

        BUYING_VENDOR_SEL = './/td[@class="results-table-center"]//a/img/@title'
        BUYING_URL = './/td//span[@class="results-price"]/a/@href'
        BUYING_DESCRIPTION_SEL = './/td[@class="item-note"]/text()'
        BUYING_PRICE_INCL_SHIPNG = './/td//span[@class="results-price"]/a/text()'



        data = response.meta['item']
        
        for seller_item in data['seller_items']:
            item = {
                'title' : data['title'],
                'pagelink' : data['pagelink'],
                'isbn_10' : data['isbn_10'],
                'isbn_13' : data['isbn_13'],
                'category' : data['category'],
                'selling_url' : seller_item['selling_url'],
                'selling_vendor' : seller_item['selling_vendor'],
                'selling_price' : seller_item['selling_price'],
            }

            # New books
            for div in response.xpath(NEW_BOOK_SEL):

                # Description extraction
                descr_list = div.xpath(BUYING_DESCRIPTION_SEL).extract()
                descr = ', '.join(descr_list)

                yield {
                'buying_vendor' : div.xpath(BUYING_VENDOR_SEL).extract_first(),
                'buying_url' : div.xpath(BUYING_URL).extract_first(),
                'condition' : 'New',
                'buying_descr' : descr,
                'buying_cost_includnig_shipping' : div.xpath(BUYING_PRICE_INCL_SHIPNG).extract_first(),
                'selling_url' : item['selling_url'],
                'selling_vendor' : item['selling_vendor'],
                'selling_price' : item['selling_price'],
                'title' : item['title'],
                'pagelink' : item['pagelink'],
                'isbn_10' : item['isbn_10'],
                'isbn_13' : item['isbn_13'],
                'category' : item['category'],
                }

            next_page_new = './/h3[@class="results-section-heading" and text()="New books: "]/following-sibling::table/tr[@class="results-table-header-row"]/th/strong/a[text()="NEXT >"]/@href'
            new_next = response.xpath(next_page_new).extract_first()
            if new_next:
                print '\nGoing to next page\n'
                yield scrapy.Request(new_next, meta={'item':data}, callback=self.bkfndr_new_nexts)
                

            # Used Books
            for div in response.xpath(USED_BOOK_SEL):

                # Description extraction
                descr_list = div.xpath(BUYING_DESCRIPTION_SEL).extract()
                descr = ', '.join(descr_list)

                yield {

                'buying_vendor' : div.xpath(BUYING_VENDOR_SEL).extract_first(),
                'buying_url' : div.xpath(BUYING_URL).extract_first(),
                'condition' : 'Used',
                'buying_descr' : descr,
                'buying_cost_includnig_shipping' : div.xpath(BUYING_PRICE_INCL_SHIPNG).extract_first(),
                'selling_url' : item['selling_url'],
                'selling_vendor' : item['selling_vendor'],
                'selling_price' : item['selling_price'],
                'title' : item['title'],
                'pagelink' : item['pagelink'],
                'isbn_10' : item['isbn_10'],
                'isbn_13' : item['isbn_13'],
                'category' : item['category'],
                }

            next_page_used = './/h3[@class="results-section-heading" and text()="Used books: "]/following-sibling::table/tr[@class="results-table-header-row"]/th/strong/a[text()="NEXT >"]/@href'
            used_next = response.xpath(next_page_used).extract_first()
            if new_next:
                yield scrapy.Request(used_next, meta={'item':data}, callback=self.bkfndr_used_nexts)


    def bkfndr_new_nexts(self, response):

        data = response.meta['item']
        
        for seller_item in data['seller_items']:
            item = {
                'title' : data['title'],
                'pagelink' : data['pagelink'],
                'isbn_10' : data['isbn_10'],
                'isbn_13' : data['isbn_13'],
                'category' : data['category'],
                'selling_url' : seller_item['selling_url'],
                'selling_vendor' : seller_item['selling_vendor'],
                'selling_price' : seller_item['selling_price'],
            }

            NEW_BOOK_SEL = './/h3[@class="results-section-heading" and text()="New books: "]/following-sibling::table/tr[@valign]'

            BUYING_VENDOR_SEL = './/td[@class="results-table-center"]//a/img/@title'
            BUYING_URL = './/td//span[@class="results-price"]/a/@href'
            BUYING_DESCRIPTION_SEL = './/td[@class="item-note"]/text()'
            BUYING_PRICE_INCL_SHIPNG = './/td//span[@class="results-price"]/a/text()'

            

            for div in response.xpath(NEW_BOOK_SEL):

                # Description extraction
                descr_list = div.xpath(BUYING_DESCRIPTION_SEL).extract()
                descr = ', '.join(descr_list)

                yield {

                'buying_vendor' : div.xpath(BUYING_VENDOR_SEL).extract_first(),
                'buying_url' : div.xpath(BUYING_URL).extract_first(),
                'condition' : 'New',
                'buying_descr' : descr,
                'buying_cost_includnig_shipping' : div.xpath(BUYING_PRICE_INCL_SHIPNG).extract_first(),
                'selling_url' : item['selling_url'],
                'selling_vendor' : item['selling_vendor'],
                'selling_price' : item['selling_price'],
                'title' : item['title'],
                'pagelink' : item['pagelink'],
                'isbn_10' : item['isbn_10'],
                'isbn_13' : item['isbn_13'],
                'category' : item['category'],
                }

            next_page_new = './/h3[@class="results-section-heading" and text()="New books: "]/following-sibling::table/tr[@class="results-table-header-row"]/th/strong/a[text()="NEXT >"]/@href'
            new_next = response.xpath(next_page_new).extract_first()
            if new_next:
                yield scrapy.Request(new_next, meta={'item':data}, callback=self.bkfndr_new_nexts)
                

    def bkfndr_used_nexts(self, response):
            
        data = response.meta['item']
        
        for seller_item in data['seller_items']:
            item = {
            'title' : data['title'],
            'pagelink' : data['pagelink'],
            'isbn_10' : data['isbn_10'],
            'isbn_13' : data['isbn_13'],
            'category' : data['category'],
            'selling_url' : seller_item['selling_url'],
            'selling_vendor' : seller_item['selling_vendor'],
            'selling_price' : seller_item['selling_price'],
            }

            USED_BOOK_SEL = './/h3[@class="results-section-heading" and text()="Used books: "]/following-sibling::table/tr[@valign]'
            
            BUYING_VENDOR_SEL = './/td[@class="results-table-center"]//a/img/@title'
            BUYING_URL = './/td//span[@class="results-price"]/a/@href'
            BUYING_DESCRIPTION_SEL = './/td[@class="item-note"]/text()'
            BUYING_PRICE_INCL_SHIPNG = './/td//span[@class="results-price"]/a/text()'


            

            # Used Books
            for div in response.xpath(USED_BOOK_SEL):

                # Description extraction
                descr_list = div.xpath(BUYING_DESCRIPTION_SEL).extract()
                descr = ', '.join(descr_list)

                yield {

                'buying_vendor' : div.xpath(BUYING_VENDOR_SEL).extract_first(),
                'buying_url' : div.xpath(BUYING_URL).extract_first(),
                'condition' : 'Used',
                'buying_descr' : descr,
                'buying_cost_includnig_shipping' : div.xpath(BUYING_PRICE_INCL_SHIPNG).extract_first(),
                'selling_url' : item['selling_url'],
                'selling_vendor' : item['selling_vendor'],
                'selling_price' : item['selling_price'],
                'title' : item['title'],
                'pagelink' : item['pagelink'],
                'isbn_10' : item['isbn_10'],
                'isbn_13' : item['isbn_13'],
                'category' : item['category'],
                }

            next_page_used = './/h3[@class="results-section-heading" and text()="Used books: "]/following-sibling::table/tr[@class="results-table-header-row"]/th/strong/a[text()="NEXT >"]/@href'
            used_next = response.xpath(next_page_used).extract_first()
            if used_next:
                yield scrapy.Request(used_next, meta={'item':data}, callback=self.bkfndr_used_nexts)